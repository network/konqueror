# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the konqueror package.
#
# SPDX-FileCopyrightText: 2024 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: konqueror\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-08-19 00:39+0000\n"
"PO-Revision-Date: 2024-03-27 22:25+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.1\n"

#: kio_bookmarks.cpp:79
#, kde-format
msgid "Root"
msgstr ""

#: kio_bookmarks.cpp:112
#, kde-format
msgid "Places"
msgstr "Llugares"

#: kio_bookmarks.cpp:206
#, kde-format
msgid "Could not find bookmarks config"
msgstr ""

#: kio_bookmarks.cpp:216
#, kde-format
msgid "Could not find bookmarks editor"
msgstr ""

#: kio_bookmarks.cpp:222
#, kde-format
msgid "Wrong request: %1"
msgstr ""

#: kio_bookmarks_html.cpp:85
#, kde-format
msgid "There are no bookmarks to display yet."
msgstr ""

#: kio_bookmarks_html.cpp:124
#, kde-format
msgid ""
"kio_bookmarks CSS file not found. Output will look ugly.\n"
"Check your installation."
msgstr ""

#: kio_bookmarks_html.cpp:133
#, kde-format
msgid "My Bookmarks"
msgstr ""
